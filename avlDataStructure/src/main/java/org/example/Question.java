package org.example;

import java.util.List;

public class Question implements Comparable<Question> {
    private String questionID;
    private String text;


    // Constructor
    public Question(String questionID, String text) {
        this.questionID = questionID;
        this.text = text;
    }

    // Getters and Setters
    public String getQuestionID() {
        return questionID;
    }

    public String getText() {
        return text;
    }


    // Implement compareTo to compare Question objects based on their questionID
    @Override
    public int compareTo(Question other) {
        return this.questionID.compareTo(other.questionID);
    }


    @Override
    public String toString() {
        return text;
    }
}

