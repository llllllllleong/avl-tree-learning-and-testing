package org.example;

public class AVLNonMutable<T extends Comparable<T>> {
    public final T node;
    public AVLNonMutable<T> leftNode;
    public AVLNonMutable<T> rightNode;
    public int balanceFactor;


    /**
     * Constructor for an empty tree
     */
    public AVLNonMutable() {
        node = null;
    }

    /**
     * Constructor for a new leaf node
     * @param node to set as this node's value.
     */
    public AVLNonMutable(T node) {
        //Insertion of null value for nodes is not permitted
        if (node == null)
            throw new IllegalArgumentException("Input cannot be null");
        this.node = node;
        this.leftNode = new EmptyTreeNonMutable<>();
        this.rightNode = new EmptyTreeNonMutable<>();
        this.balanceFactor = 0;
    }

    /**
     * Constructor for a new parent node, with child nodes
     * @param node to set as this node's value.
     * @param leftNode to set as the node's left child.
     * @param rightNode to set as the node's right child.
     */
    public AVLNonMutable(T node, AVLNonMutable<T> leftNode, AVLNonMutable<T> rightNode) {
        //Insertion of null value for nodes is not permitted
        if (node == null)
            throw new IllegalArgumentException("Input cannot be null");
        this.node = node;
        this.leftNode = leftNode;
        this.rightNode = rightNode;
    }

    /**
     * Constructor for a new parent node, with child nodes and balance factor
     * @param node to set as this node's value.
     * @param leftNode to set as the node's left child.
     * @param rightNode to set as the node's right child.
     * @param balanceFactor to set as the node's balance factor
     */
    public AVLNonMutable(T node, AVLNonMutable<T> leftNode, AVLNonMutable<T> rightNode, int balanceFactor) {
        //Insertion of null value for nodes is not permitted
        if (node == null)
            throw new IllegalArgumentException("Input cannot be null");
        this.node = node;
        this.leftNode = leftNode;
        this.rightNode = rightNode;
        this.balanceFactor = balanceFactor;
    }


    /**
     * Find the maximum height from this node.
     * @return The maximum height of either children.
     */
    public int getHeight() {
        if (this.node == null)
            throw new IllegalArgumentException("The current tree is a empty AVLNonMutable Tree");
        // Check whether leftNode or rightNode are EmptyTree
        int leftNodeHeight = leftNode instanceof EmptyTreeNonMutable ? 0 : 1 + leftNode.getHeight();
        int rightNodeHeight = rightNode instanceof EmptyTreeNonMutable ? 0 : 1 + rightNode.getHeight();
        return Math.max(leftNodeHeight, rightNodeHeight);
    }


    public void updateBalanceFactor() {
        int i = leftNode.getHeight() - rightNode.getHeight();
        this.balanceFactor = i;
    }


    /**
     * toString method
     * @return the string representation of this tree
     */
    @Override
    public String toString() {
        return "{" +
                "node=" + node +
                ", leftNode=" + leftNode +
                ", rightNode=" + rightNode +
                '}';
    }



    public AVLNonMutable<T> insert(T newNode) {
        // Ensure input is not null.
        if (newNode == null)
            throw new IllegalArgumentException("Input cannot be null");
        if (newNode.compareTo(node) > 0) {
            //If the value is bigger than current node value
            AVLNonMutable<T> a = new AVLNonMutable<>(node, leftNode, rightNode.insert(newNode));
            int currentBalance = a.getBalanceFactor();
            if (currentBalance == -2) {
                AVLNonMutable<T> b = a.leftRotate();
                currentBalance = b.getBalanceFactor();
                if (currentBalance == 2) {
                    //Right Left case
                    AVLNonMutable<T> rotatedSub =  a.rightNode;
                    rotatedSub = rotatedSub.rightRotate();
                    AVLNonMutable<T> c = new AVLNonMutable<>(node, leftNode, rotatedSub);
                    return c.leftRotate();
                }
                return b;
            }
            return a;
        } else if (newNode.compareTo(node) < 0) {
            //If the value is smaller than the current node
            AVLNonMutable<T> a = new AVLNonMutable<>(node, leftNode.insert(newNode), rightNode);
            int currentBalance = a.getBalanceFactor();
            if (currentBalance == 2) {
                AVLNonMutable<T> b = a.rightRotate();
                currentBalance = b.getBalanceFactor();
                if (currentBalance == -2) {
                    //Left Right case
                    AVLNonMutable<T> rotatedSub = a.leftNode;
                    rotatedSub = rotatedSub.leftRotate();
                    AVLNonMutable<T> c = new AVLNonMutable<>(node, rotatedSub, rightNode);
                    return c.rightRotate();
                }
                return b;
            }
            return a;
        } else {

            return new AVLNonMutable<>(node, leftNode, rightNode);
        }
    }


    public AVLNonMutable<T> leftRotate() {
        AVLNonMutable<T> current = this.rightNode;
        AVLNonMutable<T> currentLeft = current.leftNode;
        AVLNonMutable<T> currentRight = current.rightNode;

        AVLNonMutable<T> newLeft = new AVLNonMutable(node, leftNode, currentLeft);
        AVLNonMutable<T> newTree = new AVLNonMutable(current.node, newLeft, currentRight);

        return newTree;
    }

    public AVLNonMutable<T> rightRotate() {
        AVLNonMutable<T> current = this.leftNode;
        AVLNonMutable<T> currentLeft = current.leftNode;
        AVLNonMutable<T> currentRight = current.rightNode;

        AVLNonMutable<T> newRight = new AVLNonMutable(this.node, currentRight, this.rightNode);
        AVLNonMutable<T> newTree = new AVLNonMutable(current.node, currentLeft, newRight);

        return newTree;
    }

    public int getBalanceFactor() {
//        return balanceFactor;
        return leftNode.getHeight() - rightNode.getHeight();
    }


    public T search(T value) {
        if (this.node == null) {
            return null;
        }
        int compareResult = value.compareTo(this.node);
        if (compareResult == 0) {
            return this.node;
        } else if (compareResult < 0) {
            return (leftNode instanceof EmptyTreeNonMutable) ? null : leftNode.search(value);
        } else {
            return (rightNode instanceof EmptyTreeNonMutable) ? null : rightNode.search(value);
        }
    }

    public AVLNonMutable<T> findMin() {
        AVLNonMutable<T> current = this;
        while (!(current.leftNode instanceof EmptyTreeNonMutable)) {
            current = current.leftNode;
        }
        return current;
    }


    public AVLNonMutable<T> delete(T value) {
        if (node == null) {
            return this; // Value not found or tree is empty
        }

        int compareResult = value.compareTo(node);
        if (compareResult < 0) {
            if (!(leftNode instanceof EmptyTreeNonMutable)) {
                leftNode = leftNode.delete(value);
            }
        } else if (compareResult > 0) {
            if (!(rightNode instanceof EmptyTreeNonMutable)) {
                rightNode = rightNode.delete(value);
            }
        } else {
            // This is the node to be deleted
            if (leftNode instanceof EmptyTreeNonMutable && rightNode instanceof EmptyTreeNonMutable) {
                return new EmptyTreeNonMutable<>(); // Node is a leaf
            } else if (leftNode instanceof EmptyTreeNonMutable) {
                return rightNode; // Node has one child (right)
            } else if (rightNode instanceof EmptyTreeNonMutable) {
                return leftNode; // Node has one child (left)
            } else {
                // Node has two children, replace this node with the smallest node in the right subtree
                AVLNonMutable<T> minLargerNode = rightNode.findMin();
                return new AVLNonMutable<>(minLargerNode.node, leftNode, rightNode.delete(minLargerNode.node));
            }
        }
        return rebalanceAfterDeletion();
    }

    private AVLNonMutable<T> rebalanceAfterDeletion() {
        updateBalanceFactor(); // Assuming updateBalanceFactor recalculates based on children heights

        if (balanceFactor > 1) { // Left heavy
            if (leftNode.balanceFactor < 0) {
                leftNode = leftNode.leftRotate(); // Left-Right case
            }
            return rightRotate();
        } else if (balanceFactor < -1) { // Right heavy
            if (rightNode.balanceFactor > 0) {
                rightNode = rightNode.rightRotate(); // Right-Left case
            }
            return leftRotate();
        }
        return new AVLNonMutable<>(node, leftNode, rightNode);
    }




}

