package org.example;

import org.junit.Test;

import static org.junit.Assert.*;

public class AVLNonMutableTest {
    /*
        For feedback, we have provided you with a visualisation of what your implementation 'should' look like.
        Do not modify any of the existing tests.

	    We advise you write additional tests to increase the confidence of your implementation. Simply getting these
	    tests correct does not mean your solution is robust enough pass the marking tests.

	    Lastly, check out:
	    https://www.cs.usfca.edu/~galles/visualization/AVLNonMutable.html
	    for visualisation of AVL tree operations. Although please note that they allow for duplicates (they insert
	    duplicates to the right node) as opposed to not allowing duplicates.
     */

    /*
        You want to write your own tests but not sure how?
        Here is a very simple tutorial:

        1. Decide on what you want to test.
        2. Get an accurate representation of what your AVL Tree should look like. A good place to find such a
            representation is by using an AVL Tree visualiser such as the one by USFCA:
            https://www.cs.usfca.edu/~galles/visualization/AVLNonMutable.html

        3. Write an additional JUnit4 test in this AVLNonMutableTest.java class. To do this, think of a name for your
            test (standard convention has Junit test names end in test) and then use the following code (change 'Void' to 'void'):
            @Test(timeout = 1000)
            public Void someTest() {
                // Your assertions go here
            }

        4. Create an instance of the AVLNonMutable class and provide the inputs IN THE CORRECT ORDER. The order here
            matters as it will affect rotations. Try to input nodes in the same order that the representation in (2)
            does. To input nodes simply use the code:
            AVLNonMutable<Integer> avl = new AVLNonMutable<>(<root node>)
                .insert(<second node>)
                .insert(<third node>)
                .insert(<forth node>);
            you can continue this code to insert as many nodes as you want.

        The next steps will depend on what you want to test:

        5a. For height, balance factor, or node tests: Calculate what your expecting the height/balance/node to be
            according to your accurate representation in (2). Then use the code:
            assertEquals(<expected node>, avl.getBalanceFactor() or avl.getHeight() or avl.node);

        5b. For testing whether something is or is not null, use the code:
            assertNull(<input>); or assertNotNull(<input>);

        5c. For testing the overall structure of the code you may choose to use 'assertEquals' and provide a string.
            However, please know that if even a single character mismatches between expectation and actual, the test
            will fail.

            The Tree 'toString()' output is recursive and the 'toString()' method can be found in Tree.java.
            {node=<root node node>, leftNode={}, rightNode={}}
            Below is an example of such a test:
            assertEquals("{node=5, leftNode={}, rightNode={}}", avl.toString());
     */

    @Test(timeout = 1000)
    public void immutableTest1() {
        // Simple check if the implementation is immutable. relies on insert() implementation
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(5);
        avl.insert(10);
        String expected = "{node=5, leftNode={}, rightNode={}}";
        assertEquals("\nAVL tree implementation is not immutable" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                expected,avl.toString());
    }

    @Test(timeout = 1000)
    public void immutableTest2() {
        // more complex check if the implementation is immutable, relies on insert() implementation
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(1);
        avl = avl.insert(15);
        avl = avl.insert(45);
        avl.insert(10);
        avl.insert(50);
        avl.insert(3);
        String expected = "{node=15, leftNode={node=1, leftNode={}, rightNode={}}, rightNode={node=45, leftNode={}, rightNode={}}}";
        assertEquals("\nAVL tree implementation is not immutable" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                expected,avl.toString());
    }

    @Test(timeout = 1000)
    public void insertInOrderTest() {
        // Simply check if the insertion correctly places nodes (no rotation check).
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(5);
        avl = avl.insert(10);
        String expected = "{node=5, leftNode={}, rightNode={node=10, leftNode={}, rightNode={}}}";
        assertNotNull(
                "\nInsertion does not properly position nodes" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.rightNode.node);
        assertEquals(
                "\nInsertion does not properly position nodes" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl
                ,
                10, (int) avl.rightNode.node);

        avl = avl.insert(1);
        expected = "{node=5, leftNode={node=1, leftNode={}, rightNode={}}, rightNode={node=10, leftNode={}, rightNode={}}}";
        assertNotNull(
                "\nInsertion does not properly position nodes" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.leftNode.node);
        assertEquals(
                "\nInsertion does not properly position nodes" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                1, (int) avl.leftNode.node);
    }

    @Test(timeout = 1000)
    public void insertDuplicateTest() {
        // As per the implementation requirements, duplicates should be ignored.
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(5).insert(5);
        String expected = "{node=5, leftNode={}, rightNode={}}";
        assertEquals(
                "\nInsertion does not properly position nodes" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                0, avl.getHeight());

        // Double checking encase anyone changes height output.
        assertNull("\nInsertion does not properly handle duplicates" +
                "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl, avl.leftNode.node);

        assertNull("\nInsertion does not properly handle duplicates" +
                "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl, avl.rightNode.node);
    }

    @Test(timeout = 1000)
    public void leftRotateTest() {
        // constructing the tree manually to avoid using the insert method
        AVLNonMutable<Integer> avlRightRightChild = new AVLNonMutable<>(10);
        AVLNonMutable<Integer> avlRightChild = new AVLNonMutable<>(8,new AVLNonMutable<>(),avlRightRightChild);
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(5,new AVLNonMutable<>(),avlRightChild).leftRotate();
        String expected = "{node=8, leftNode={node=5, leftNode={}, rightNode={}}, rightNode={node=10, leftNode={}, rightNode={}}}";
        // Check root node
        assertNotNull(
                "\nLeft rotation failed" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.node);
        assertEquals(
                "\nLeft rotation failed" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                8, (int) avl.node);

        // Check left child node
        assertNotNull(
                "\nLeft rotation failed" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.leftNode.node);
        assertEquals(
                "\nLeft rotation failed" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                5, (int) avl.leftNode.node);

        // Check right child node
        assertNotNull(
                "\nLeft rotation failed" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.rightNode.node);
        assertEquals(
                "\nLeft rotation failed" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                10, (int) avl.rightNode.node);
    }

    @Test(timeout = 1000)
    public void rightRotateTest() {
        // constructing the tree manually to avoid using the insert method
        AVLNonMutable<Integer> avlLeftLeftChild = new AVLNonMutable<>(3);
        AVLNonMutable<Integer> avlLeftChild = new AVLNonMutable<>(6,avlLeftLeftChild,new AVLNonMutable<>());
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(10,avlLeftChild,new AVLNonMutable<>()).rightRotate();
        String expected = "{node=6, leftNode={node=3, leftNode={}, rightNode={}}, rightNode={node=10, leftNode={}, rightNode={}}}";
        // Check root node
        assertNotNull(
                "\nRight rotation failed" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.node);
        assertEquals(
                "\nRight rotation failed" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                6, (int) avl.node);

        // Check left child node
        assertNotNull(
                "\nRight rotation failed" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.leftNode.node);
        assertEquals(
                "\nRight rotation failed" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                3, (int) avl.leftNode.node);

        // Check right child node
        assertNotNull(
                "\nRight rotation failed" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.rightNode.node);
        assertEquals(
                "\nRight rotation failed" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                10, (int) avl.rightNode.node);
    }

    // NOTE: Below test relies on insert() method
    @Test(timeout = 1000)
    public void balanceFactorTest() {
        // Ensure insertion results in balanced tree.
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(5).insert(10).insert(20);
        String expected = "{node=10, leftNode={node=5, leftNode={}, rightNode={}}, rightNode={node=20, leftNode={}, rightNode={}}}";
        assertEquals(
                "\nInsertion does not properly balance tree (must left rotate)" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                0, avl.getBalanceFactor()
        );

        avl = avl.insert(22).insert(21);
        expected = "{node=10, leftNode={node=5, leftNode={}, rightNode={}}, rightNode={node=21, leftNode={node=20, leftNode={}, rightNode={}}, rightNode={node=22, leftNode={}, rightNode={}}}}";
        assertEquals(
                "\nInsertion does not properly balance tree (must left, right, left rotate)" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                -1, avl.getBalanceFactor()
        );

        avl = avl.insert(23);
        expected = "{node=21, leftNode={node=10, leftNode={node=5, leftNode={}, rightNode={}}, rightNode={node=20, leftNode={}, rightNode={}}}, rightNode={node=22, leftNode={}, rightNode={node=23, leftNode={}, rightNode={}}}}";
        assertEquals(
                "\nInsertion does not properly balance tree (must left, right, left, left rotate)" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                0, avl.getBalanceFactor()
        );

        //Advanced
        avl = new AVLNonMutable<>(10)
                .insert(5)
                .insert(6)
                .insert(4)
                .insert(7)
                .insert(2)
                .insert(1)
                .insert(0)
                .insert(3);
        expected = "{node=6, leftNode={node=2, leftNode={node=1, leftNode={node=0, leftNode={}, rightNode={}}, rightNode={}}, rightNode={node=4, leftNode={node=3, leftNode={}, rightNode={}}, rightNode={node=5, leftNode={}, rightNode={}}}}, rightNode={node=10, leftNode={node=7, leftNode={}, rightNode={}}, rightNode={}}}";
        assertEquals(
                "\nInsertion does not properly balance tree (must left, right, right, right, left, right rotate)" +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                1, avl.getBalanceFactor()
        );
    }

    // NOTE: Below test relies on insert() method
    @Test(timeout = 1000)
    public void advancedRotationsInsertionTest() {
        // Cause a situation with a RR, RL, LL or LR rotation is required.
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(14)
                .insert(17)
                .insert(11)
                .insert(7)
                .insert(53)
                .insert(4)
                .insert(13)
                .insert(12)
                .insert(8)
                .insert(60)
                .insert(19)
                .insert(16)
                .insert(20);

        String expected = "{node=14, leftNode={node=11, leftNode={node=7, leftNode={node=4, leftNode={}, rightNode={}}, rightNode={node=8, leftNode={}, rightNode={}}}, rightNode={node=12, leftNode={}, rightNode={node=13, leftNode={}, rightNode={}}}}, rightNode={node=19, leftNode={node=17, leftNode={node=16, leftNode={}, rightNode={}}, rightNode={}}, rightNode={node=53, leftNode={node=20, leftNode={}, rightNode={}}, rightNode={node=60, leftNode={}, rightNode={}}}}}";
        assertNotNull(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.node);
        assertNotNull(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.leftNode.node);
        assertNotNull(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.rightNode.node);
        assertEquals(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                0, avl.getBalanceFactor()
        );
        assertEquals(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                14, (int) avl.node
        );
        assertEquals(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                11, (int) avl.leftNode.node
        );
        assertEquals(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                19, (int) avl.rightNode.node
        );

        // Another double rotation requiring test.
        avl = new AVLNonMutable<>(40)
                .insert(20)
                .insert(10)
                .insert(25)
                .insert(30)
                .insert(22)
                .insert(50);

        expected = "{node=25, leftNode={node=20, leftNode={node=10, leftNode={}, rightNode={}}, rightNode={node=22, leftNode={}, rightNode={}}}, rightNode={node=40, leftNode={node=30, leftNode={}, rightNode={}}, rightNode={node=50, leftNode={}, rightNode={}}}}";
        assertNotNull(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.node);
        assertNotNull(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.leftNode.node);
        assertNotNull(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                avl.rightNode.node);
        assertEquals(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                0, avl.getBalanceFactor()
        );
        assertEquals(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                25, (int) avl.node
        );
        assertEquals(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                20, (int) avl.leftNode.node
        );
        assertEquals(
                "\nInsertion cannot handle either right right, right left, left left or left right double rotations." +
                        "\nYour AVL tree should look like: " + expected + "\nBut it actually looks like: " + avl,
                40, (int) avl.rightNode.node
        );
    }

    @Test(timeout = 1000)
    public void complexDoubleRotationTest() {
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(40)
                .insert(20)
                .insert(10)
                .insert(30)
                .insert(25)
                .insert(22);
        String expected = "{node=25, leftNode={node=20, leftNode={node=10, leftNode={}, rightNode={}}, rightNode={node=22, leftNode={}, rightNode={}}}, rightNode={node=30, leftNode={}, rightNode={node=40, leftNode={}, rightNode={}}}}";
        assertEquals(
                "\nComplex double rotations not handled correctly." +
                        "\nExpected tree structure: " + expected +
                        "\nActual tree structure: " + avl.toString(),
                expected, avl.toString());
    }


    @Test(timeout = 2500)
    public void largeScaleInsertTest() {
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(500);
        int[] values = new int[2500]; // Insert 1000 values
        for (int i = 1; i <= 2500; i++) {
            avl = avl.insert(i);
        }
        // Check if the height is log(n) which is a property of AVL trees
        assertTrue("Tree is not balanced as expected for 1000 nodes.",
                Math.log(2500)/Math.log(2) >= avl.getHeight());
    }





    public void testSearchExistence() {
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(50)
                .insert(30).insert(70).insert(20).insert(40).insert(60).insert(80);
        assertNotNull("Search for an existing value should return non-null.", avl.search(60));
    }

    @Test
    public void testSearchNonExistence() {
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(50)
                .insert(30).insert(70);
        assertNull("Search for a non-existing value should return null.", avl.search(25));
    }

    @Test
    public void testDeleteLeafNode() {
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(50)
                .insert(30).insert(70).insert(20).insert(40).insert(60).insert(80);
        avl = avl.delete(20);  // Deleting leaf node
        assertNull("Leaf node 20 should be deleted.", avl.search(20));
        assertEquals("Deleting leaf node should not unbalance tree.", 0, avl.getBalanceFactor());
    }

    @Test
    public void testDeleteNodeWithOneChild() {
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(50)
                .insert(30).insert(70).delete(70);  // 70 is a node with one child
        assertNull("Node 70 should be deleted.", avl.search(70));
    }

    @Test
    public void testDeleteNodeWithTwoChildren() {
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(50)
                .insert(30).insert(70).insert(60).insert(80);
        avl = avl.delete(70);  // Node with two children
        assertNull("Node 70 should be replaced.", avl.search(70));
        assertNotNull("In-order successor 80 should now be where 70 was.", avl.search(80));
    }

    @Test
    public void testDeleteRootNode() {
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(50)
                .insert(30).insert(70);
        avl = avl.delete(50);  // Deleting the root
        assertNull("Root node 50 should be deleted.", avl.search(50));
        assertNotNull("New root (either 30 or 70) should exist.", avl.search(30) != null || avl.search(70) != null);
    }

    @Test
    public void testSequentialDeletes() {
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(50)
                .insert(30).insert(70).insert(20).insert(40).insert(60).insert(80);
        int[] deletes = {50, 20, 70, 60};
        for (int delete : deletes) {
            avl = avl.delete(delete);
        }
        assertNull("All deleted nodes should be absent from the tree.", avl.search(50));
        assertNotNull("Remaining nodes should still be findable.", avl.search(30));
    }

    @Test
    public void testDeleteOnEmptyTree() {
        AVLNonMutable<Integer> avl = new AVLNonMutable<>();
        avl = avl.delete(50);  // Trying to delete from an empty tree
        assertNull("Deleting from an empty tree should return null or an empty tree.", avl.node);
    }

    @Test
    public void stressTestRandomDeletes() {
        int size = 1000;
        AVLNonMutable<Integer> avl = new AVLNonMutable<>(size / 2);
        for (int i = 1; i < size; i++) {
            if (i != size / 2) avl = avl.insert(i);
        }
        java.util.Random rand = new java.util.Random();
        while (size > 1) {
            int toDelete = rand.nextInt(size - 1) + 1;
            avl = avl.delete(toDelete);
            assertNull("Deleted node " + toDelete + " should not be found.", avl.search(toDelete));
            size--;
        }
    }



}
