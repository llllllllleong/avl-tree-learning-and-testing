package org.example;

import org.junit.Test;

import static org.junit.Assert.*;

public class AVLMutableTest {

    @Test
    public void testEmptyTreeProperties() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        assertTrue("Tree should be empty", avl.isEmpty());
        assertEquals("Empty tree size should be 0", 0, avl.size());
        assertNull("Root should be null in an empty tree", avl.root);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInsertNull() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(null);
    }

    @Test
    public void testSingleInsert() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(10);
        assertFalse("Tree should not be empty after insert", avl.isEmpty());
        assertEquals("Size should be 1 after one insert", 1, avl.size());
        assertNotNull("Root should not be null after insert", avl.root);
        assertEquals("Root object should be 10", (Integer) 10, avl.root.object);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInsertDuplicate() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(10);
        avl.insert(10); // Should throw exception
    }

    @Test
    public void testHeightUpdateOnInsert() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(20);
        avl.insert(10);
        avl.insert(30);
        assertEquals("Height of the root should be 1", 1, avl.root.height);
    }

    @Test
    public void testRightRotation() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(30);
        avl.insert(20);
        avl.insert(10); // Trigger right rotation
        assertEquals("Root should be 20 after right rotation", (Integer) 20, avl.root.object);
    }

    @Test
    public void testLeftRotation() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(10);
        avl.insert(20);
        avl.insert(30); // Trigger left rotation
        assertEquals("Root should be 20 after left rotation", (Integer) 20, avl.root.object);
    }

    @Test
    public void testLeftRightRotation() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(30);
        avl.insert(10);
        avl.insert(20); // Trigger left-right rotation
        assertEquals("Root should be 20 after left-right rotation", (Integer) 20, avl.root.object);
    }

    @Test
    public void testRightLeftRotation() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(10);
        avl.insert(30);
        avl.insert(20); // Trigger right-left rotation
        assertEquals("Root should be 20 after right-left rotation", (Integer) 20, avl.root.object);
    }

    @Test
    public void testContains() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(20);
        avl.insert(10);
        avl.insert(30);
        assertTrue("Should contain 10", avl.contains(10));
        assertFalse("Should not contain 40", avl.contains(40));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testContainsNull() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.contains(null); // Should throw exception
    }

    @Test
    public void testDeleteAll() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(20);
        avl.insert(10);
        avl.insert(30);
        avl.deleteAll();
        assertTrue("Tree should be empty after delete all", avl.isEmpty());
        assertEquals("Tree size should be 0 after delete all", 0, avl.size());
    }

    @Test
    public void testMultipleInsertionsAndBalancing() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        // Inserting values to create a complex tree that requires multiple rotations
        int[] values = {50, 30, 70, 20, 40, 60, 80, 15, 25, 35, 45, 55, 65, 75, 85};
        for (int value : values) {
            avl.insert(value);
        }
        assertEquals("Tree should have 15 nodes after insertions", 15, avl.size());
        // Expect the tree to be balanced
        assertTrue("Tree should be balanced", avl.getBalanceFactor(avl.root) < 2);
    }

    @Test
    public void testInsertionsWithMultipleRotations() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        // Sequence of insertions that trigger multiple rotations
        avl.insert(40);
        avl.insert(20);
        avl.insert(60);
        avl.insert(10);
        avl.insert(30);
        avl.insert(50);
        avl.insert(70);
        avl.insert(25); // Triggers complex rotations
        assertEquals("Root should be correctly balanced and possibly changed", (Integer) 40, avl.root.object);
        assertTrue("Height should be correctly updated", avl.root.height == 3);
    }


    @Test
    public void testLargeScaleBalancing() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        // Insert a large number of elements to test tree's overall balancing on large scale
        for (int i = 1; i <= 1000; i++) {
            avl.insert(i);
        }
        assertTrue("Tree should maintain balancing property, height should be log(n)", avl.getHeight(avl.root) <= (Math.log(1000) / Math.log(2)));
    }


    @Test
    public void testDeleteLeafNode() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(20);
        avl.insert(10);
        avl.insert(30);
        avl.delete(10);
        assertFalse("Deleted node should no longer exist", avl.contains(10));
        assertEquals("Tree should have 2 nodes after deletion", 2, avl.size());
        assertTrue("Balance factor should be valid", Math.abs(avl.getBalanceFactor(avl.root)) <= 1);
    }

    @Test
    public void testDeleteNodeWithOneChild() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(20);
        avl.insert(10);
        avl.insert(30);
        avl.insert(5);
        assertEquals("Tree height should update correctly", 2, avl.getHeight(avl.root));
        avl.delete(10);
        assertFalse("Deleted node 10 should not exist", avl.contains(10));
        assertTrue("Node 5 should exist as it is the child of deleted node 10", avl.contains(5));
        assertEquals("Tree height should update correctly", 1, avl.getHeight(avl.root));
        assertTrue("Balance factor should be valid", Math.abs(avl.getBalanceFactor(avl.root)) <= 1);
    }

    @Test
    public void testDeleteNodeWithTwoChildren() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(50);
        avl.insert(30);
        avl.insert(70);
        avl.insert(20);
        avl.insert(40);
        avl.delete(30);
        assertFalse("Deleted node 30 should not exist", avl.contains(30));
        assertTrue("Node 40 should become the new child of 50", avl.contains(40));
        assertTrue("Balance factor should be valid", Math.abs(avl.getBalanceFactor(avl.root)) <= 1);
    }

    @Test
    public void testDeleteRootNode() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        avl.insert(50);
        avl.insert(30);
        avl.insert(70);
        avl.delete(50);
        assertFalse("Deleted node 50 should not exist", avl.contains(50));
        assertTrue("One of 30 or 70 should be the new root", avl.contains(30) || avl.contains(70));
        assertTrue("Balance factor should be valid", Math.abs(avl.getBalanceFactor(avl.root)) <= 1);
    }

    @Test
    public void testBalanceAfterMultipleDeletions() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        int[] numbers = {50, 25, 75, 10, 30, 60, 80, 5, 15, 27, 55, 65, 77, 85};
        for (int num : numbers) {
            avl.insert(num);
        }
        for (int num : new int[]{30, 75, 10, 50, 27}) {
            avl.delete(num);
        }
        avl.printTree();
        assertTrue("Remaining tree should still be balanced", Math.abs(avl.getBalanceFactor(avl.root)) <= 1);
        assertEquals("Remaining tree should have correct size", numbers.length - 5, avl.size());
    }

    @Test
    public void testComplexTreeDeletion() {
        AVLMutable<Integer> avl = new AVLMutable<>();
        // Insert elements to form a complex tree
        int[] inserts = {50, 30, 70, 20, 40, 60, 80, 15, 25, 35, 45, 55, 65, 75, 85};
        for (int value : inserts) {
            avl.insert(value);
        }
        // Deleting multiple elements
        int[] deletes = {50, 70, 15, 25, 35, 45, 55};
        for (int del : deletes) {
            avl.delete(del);
        }
        // Check if tree is still balanced after each deletion
        for (int remaining : new int[]{30, 40, 60, 65, 75, 80, 85}) {
            assertTrue("Node " + remaining + " should still exist", avl.contains(remaining));
        }
    }
}
