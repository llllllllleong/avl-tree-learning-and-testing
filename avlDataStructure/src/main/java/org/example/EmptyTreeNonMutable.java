package org.example;

public class EmptyTreeNonMutable<T extends Comparable<T>> extends AVLNonMutable<T> {

    /**
     * Height is defined as the number of edges from current node to leaf
     * A leaf node has height 0, therefore an empty tree cannot have a height of 0
     * Therefore we return -1.
     */
    @Override
    public int getHeight() {
        return -1;
    }

    /**
     * toString method
     * @return the string representation of this tree
     */
    @Override
    public String toString() {
        return "{}";
    }

    @Override
    public AVLNonMutable<T> insert(T node) {
        return new AVLNonMutable<T>(node);
    }




}
