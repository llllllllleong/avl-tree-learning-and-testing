package org.example;



import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AVLMutable<T extends Comparable<T>> {
    public AVLNode<T> root;
    int nodeCount = 0;


    /**
     * Constructor for an empty AVLMutable
     */
    public AVLMutable() {
        this.root = null;
    }

    /**
     * Delete everything in the current AVLMutable
     */
    public void deleteAll() {
        this.root = null;
        nodeCount = 0;
    }


    public boolean isEmpty() {
        return (this.root == null);
    }

    public int getHeight(AVLNode<T> n) {
        return (n == null) ? -1 : n.height;
    }

    public int getBalanceFactor(AVLNode<T> n) {
        if (n == null) return 0;
        return getHeight(n.leftNode) - getHeight(n.rightNode);
    }

    public int size() {
        return nodeCount;
    }

    public List<T> getAll() {
        List<T> output = new ArrayList<>();
        getAll(this.root, output);
        return output;
    }
    public void getAll(AVLNode<T> node, List<T> output) {
        if (node == null) return;
        output.add(node.getObject());
        getAll(node.getLeftNode(), output);
        getAll(node.getRightNode(), output);
    }


    /**
     * Insert an object into the AVL tree
     *
     * @param object the object to be inserted
     * @throws IllegalArgumentException if the object is null or the value exists in the tree
     *                                  <p>
     *                                  This insertion method is a wrapper method for the recursive insertion method
     *                                  The recursive insertion method is an overloaded method, with the same name
     *                                  This method simply calls on the recursive method, and reassigns the root node to
     *                                  the output of the recursive method
     */
    public void insert(T object, Integer key) {
        if (root == null) {
            root = new AVLNode<>(object, key);
        } else {
            //AVLNode class will check if the object to be inserted, is null, and throw an exception if it is
            root = insert(root, object, key);
        }
        nodeCount++;
    }

    /**
     * Overloaded method for inserting an object
     *
     * @param object the object to be inserted
     * @param node   the current node being checked
     * @throws IllegalArgumentException if the object is null or the value exists in the tree
     */
    public AVLNode<T> insert(AVLNode<T> node, T object, Integer key) {
        if (node == null) {
            //The current AVLMutable is an empty tree
            return new AVLNode<>(object, key);
        } else {
            //The current AVLMutable is not an empty tree
            int compareResult = key - node.getKey();
            if (compareResult > 0) {
                //Insert to the right
                node.rightNode = insert(node.rightNode, object, key);
                //Check balance factor
                if (getBalanceFactor(node) == -2) {
                    compareResult = key - node.rightNode.getKey();
                    if (compareResult > 0) {
                        //Right-Right Case
                        node = leftRotate(node);
                    } else {
                        //Right-Left Case
                        node = leftRotateDouble(node);
                    }
                }
            } else if (compareResult < 0) {
                //Insert to the left
                node.leftNode = insert(node.leftNode, object, key);
                //Check balance factor
                if (getBalanceFactor(node) == 2) {
                    compareResult = key - node.leftNode.getKey();
                    if (compareResult < 0) {
                        //Left-Left Case
                        node = rightRotate(node);
                    } else {
                        //Left-Right Case
                        node = rightRotateDouble(node);
                    }
                }
            } else {
                //Object already exists in the tree
//                throw new IllegalArgumentException("Object already exists in the tree");
            }
            node.height = Math.max(getHeight(node.leftNode), getHeight(node.rightNode)) + 1;
            return node;
        }
    }


    public AVLNode<T> leftRotate(AVLNode<T> node) {
        AVLNode<T> newRoot = node.rightNode;
        node.rightNode = newRoot.leftNode;
        newRoot.leftNode = node;
        node.height = Math.max(getHeight(node.leftNode), getHeight(node.rightNode)) + 1;
        newRoot.height = Math.max(getHeight(newRoot.rightNode), node.height) + 1;
        return newRoot;
    }

    public AVLNode<T> leftRotateDouble(AVLNode<T> node) {
        node.rightNode = rightRotate(node.rightNode);
        return leftRotate(node);
    }

    public AVLNode<T> rightRotate(AVLNode<T> node) {
        AVLNode<T> newRoot = node.leftNode;
        node.leftNode = newRoot.rightNode;
        newRoot.rightNode = node;
        node.height = Math.max(getHeight(node.leftNode), getHeight(node.rightNode)) + 1;
        newRoot.height = Math.max(getHeight(newRoot.leftNode), node.height) + 1;
        return newRoot;
    }

    public AVLNode<T> rightRotateDouble(AVLNode<T> node) {
        node.leftNode = leftRotate(node.leftNode);
        return rightRotate(node);
    }


    public boolean contains(T object) {
        if (object == null)
            throw new IllegalArgumentException("contains is searching for a null object, " +
                    "AVLMutable should not contain null objects");
        return contains(root, object);
    }

    public boolean contains(AVLNode<T> node, T object) {
        if (node == null) return false;
        int compareResult = object.compareTo(node.object);
        if (compareResult > 0) return contains(node.rightNode, object);
        if (compareResult < 0) return contains(node.leftNode, object);
        return true;
    }

    public boolean containsKey(Integer key) {
        if (key == null)
            throw new IllegalArgumentException("contains is searching for a null key object, " +
                    "AVLMutable should not contain null objects");
        return containsKey(root, key);
    }

    public boolean containsKey(AVLNode<T> node, Integer key) {
        if (node == null) return false;
        int compareResult = key - node.getKey();
        if (compareResult > 0) return containsKey(node.rightNode, key);
        if (compareResult < 0) return containsKey(node.leftNode, key);
        return true;
    }

    public T get(Integer key) {
        if (key == null)
            throw new IllegalArgumentException("get is getting a null key, " +
                    "AVLMutable should not contain null objects");
        return get(root, key);
    }

    public T get(AVLNode<T> node, Integer key) {
        if (node == null) return null;
        int compareResult = key - node.getKey();
        if (compareResult > 0) return get(node.rightNode, key);
        if (compareResult < 0) return get(node.leftNode, key);
        return node.getObject();
    }


    public boolean safeDelete(Integer key) {
        if (!containsKey(root, key)) return false;
        List<T> all = getAll();
        deleteAll();
        boolean foundDeleteObject = false;
        for (T q : all) {
            if (!Objects.equals(((Question) q).getKey(), key)) insert(q, ((Question) q).getKey());
            else foundDeleteObject = true;
        }
        return foundDeleteObject;
    }

    public boolean delete(Integer key) {
        if (root == null)
            throw new IllegalArgumentException("The root is null, cannot delete from an empty tree");
        if (key == null)
            throw new IllegalArgumentException("Key null object");
        if (!containsKey(key)) return false;
        int compareResult = key - root.getKey();
        if (compareResult == 0) {
            //Base case: deleting the root
            if (root.leftNode == null && root.rightNode == null) {
                deleteAll();
            } else {
                AVLMutable<T> newTree = new AVLMutable<>();
                transferTree(newTree, root.leftNode, key);
                transferTree(newTree, root.rightNode, key);
                root = newTree.root;
                nodeCount = newTree.nodeCount;
            }
        } else {
            AVLMutable<T> newTree = new AVLMutable<>();
            transferTree(newTree, root, key);
            root = newTree.root;
            nodeCount = newTree.nodeCount;
        }
        return true;
    }

    public void transferTree(AVLMutable<T> newTree, AVLNode<T> node, Integer deleteKey) {
        if (node == null) return;
        int compareResult = deleteKey - node.getKey();
        if (compareResult != 0) newTree.insert(node.object, node.getKey());
        transferTree(newTree, node.leftNode, deleteKey);
        transferTree(newTree, node.rightNode, deleteKey);
    }


    public static class AVLNode<T extends Comparable<T>> {
        public T object;
        public Integer key;
        public AVLNode<T> leftNode;
        public AVLNode<T> rightNode;
        public int height;

        public AVLNode<T> getLeftNode() {
            return leftNode;
        }

        public void setLeftNode(AVLNode<T> leftNode) {
            this.leftNode = leftNode;
        }

        public AVLNode<T> getRightNode() {
            return rightNode;
        }

        public void setRightNode(AVLNode<T> rightNode) {
            this.rightNode = rightNode;
        }

        public T getObject() {
            return object;
        }
        public Integer getKey() {
            return this.key;
        }

        /**
         * Constructor for a leaf node
         * Leaf nodes have a height of 0
         */
        public AVLNode(T object, Integer key) {
            //Do not allow leaf nodes to be created with null value
            if (object == null)
                throw new IllegalArgumentException("Input cannot be null");
            this.object = object;
            this.height = 0;
            this.key = key;
            leftNode = null;
            rightNode = null;
        }
    }

    public AVLNode<T> getRoot() {
        return root; // Provide access to the root node
    }
}



